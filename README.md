# playwright-pom

This mini-project focuses on utilising a page object model structure (POM) for 
UI tests within Playwright python, and the pytest plugin for Playwright.
It utilises the following online UI automation site as the base for its tests:
  - https://www.saucedemo.com/

This project has been interesting because pytest plugin handles a lot of the 
boilerplate you'd normally need in POM design. This means it's much faster
to get up and running with a suite of tests in a CI/CD pipeline.

Here, I've only touched on the basics, but the framework is intuitive enough to allow
for very quick expansion in coverage.

## Dependencies

- playwright
- pytest
- pytest-playwright

## Getting Started

For Windows users:
```
python -m venv venv
.\venv\Scripts\activate
pip install -r requirements.txt
```

For Linux users:
```
python3.10 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

Your venv should now be set up with all of the necessary tools to get started in
running the tests. 

The project uses `pytest` as the test runner, with `flake8` and `black` as linters of
choice.
Each of these has their own help menu, or can be run with (against the config settings
outlined in the .flake8 and pytest.ini files):
```
pytest
flake8
black ./
```
