from pages.products import Products


def test_products_elements(page):
    products_page = Products(page)
    products_page.login_user("standard")
    item1 = products_page.page.locator(".inventory_item_name")
    item1_text = item1.first.inner_text()
    assert item1_text == "Sauce Labs Backpack"
    assert products_page.add_backpack_button.is_visible()


def test_item_view(page):
    products_page = Products(page)
    products_page.login_user("standard")
    products_page.page.click("text=Sauce Labs Onesie")
    assert page.url == "https://www.saucedemo.com/inventory-item.html?id=2"
    price = products_page.page.locator(".inventory_details_price").inner_text()
    assert price == "$7.99"


def test_add_to_cart(page):
    products_page = Products(page)
    products_page.login_user("standard")
    products_page.add_backpack_button.click()
    remove_button = products_page.page.locator("#remove-sauce-labs-backpack")
    assert remove_button.is_visible()
    cart_badge = products_page.page.locator(".shopping_cart_badge")
    assert cart_badge.inner_text() == "1"


def test_cart_increments(page):
    products_page = Products(page)
    products_page.login_user("standard")
    products_page.add_backpack_button.click()
    products_page.add_bike_light_button.click()
    products_page.add_bolt_shirt_button.click()
    products_page.add_fleece_button.click()
    products_page.add_onesie_button.click()
    products_page.add_test_shirt_button.click()
    cart_badge = products_page.page.locator(".shopping_cart_badge")
    assert cart_badge.inner_text() == "6"


def test_remove_from_cart(page):
    products_page = Products(page)
    products_page.login_user("standard")
    products_page.add_backpack_button.click()
    products_page.page.click("#remove-sauce-labs-backpack")
    assert products_page.page.locator(".shopping_cart_badge").is_hidden()


def test_sorting_menu(page):
    products_page = Products(page)
    products_page.login_user("standard")
    products_page.select_sort_option(index=1)
    item1_text = products_page.item_titles.first.inner_text()
    assert item1_text == "Test.allTheThings() T-Shirt (Red)"

    # Selecting an item removes the element from the DOM and re-renders it?
    # So you can't select it twice
    products_page.select_sort_option(index=3)
    item2_text = products_page.item_titles.first.inner_text()
    assert item2_text == "Sauce Labs Fleece Jacket"

    # Selecting the first index breaks the universe?
    products_page.select_sort_option(value="az")
    item3_text = products_page.item_titles.first.inner_text()
    assert item3_text == "Sauce Labs Backpack"
