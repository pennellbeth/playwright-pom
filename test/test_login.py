from pages.login import Login
from pages.products import Products

user = {"username": "standard_user", "password": "secret_sauce"}


def test_login_elements(page):
    login_page = Login(page)
    login_page.page.goto("https://www.saucedemo.com/")
    # TODO: Find the png attribute to assert on
    assert login_page.page.title() == "Swag Labs"
    assert login_page.username_field.is_visible()
    assert login_page.password_field.is_visible()
    assert login_page.login_button.is_visible()


def test_valid_login(page):
    login_page = Login(page)
    products_page = Products(page)
    login_page.login_user("standard")
    visible = products_page.inventory_container.last.is_visible()
    assert visible and "inventory" in page.url


def test_invalid_login(page):
    login_page = Login(page)
    login_page.login_user("typo")
    error_message = login_page.page.locator("[data-test='error']").inner_text()
    assert error_message == (
        "Epic sadface: Username and password do not match any user in this service"
    )


def test_locked_out_user(page):
    login_page = Login(page)
    login_page.login_user("locked-out")
    error_message = login_page.page.locator("[data-test='error']").inner_text()
    assert error_message == ("Epic sadface: Sorry, this user has been locked out.")


def test_logout(page):
    login_page = Login(page)
    products_page = Products(page)
    login_page.login_user("standard")
    products_page.burger_menu.click()
    products_page.page.click("#logout_sidebar_link")
    assert page.url == "https://www.saucedemo.com/"
