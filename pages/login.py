credentials = {
    "standard": {"username": "standard_user", "password": "secret_sauce"},
    "locked-out": {"username": "locked_out_user", "password": "secret_sauce"},
    "problem": {"username": "problem_user", "password": "secret_sauce"},
    "perf-glitch": {"username": "performance_glitch_user", "password": "secret_sauce"},
    "typo": {"username": "standard_user", "password": "secret_sace"},
}


class Login:
    def __init__(self, page):
        self.page = page

    @property
    def login_form(self):
        return self.page.wait_for_selector("#login_button_container")

    @property
    def username_field(self):
        return self.login_form.wait_for_selector("#user-name")

    @property
    def password_field(self):
        return self.login_form.wait_for_selector("#password")

    @property
    def login_button(self):
        return self.login_form.wait_for_selector("#login-button")

    def submit_login_form(self, user):
        self.username_field.fill(user["username"])
        self.password_field.fill(user["password"])
        self.login_button.click()

    def login_user(self, user_type):
        self.page.goto("https://www.saucedemo.com/")
        self.submit_login_form(credentials[user_type])
