from pages.login import Login


class Products(Login):
    @property
    def inventory_container(self):
        return self.page.locator("#inventory_container")

    @property
    def add_backpack_button(self):
        return self.page.locator("#add-to-cart-sauce-labs-backpack")

    @property
    def add_bike_light_button(self):
        return self.page.locator("#add-to-cart-sauce-labs-bike-light")

    @property
    def add_bolt_shirt_button(self):
        return self.page.locator("#add-to-cart-sauce-labs-bolt-t-shirt")

    @property
    def add_fleece_button(self):
        return self.page.locator("#add-to-cart-sauce-labs-fleece-jacket")

    @property
    def add_onesie_button(self):
        return self.page.locator("#add-to-cart-sauce-labs-onesie")

    @property
    def add_test_shirt_button(self):
        return self.page.locator("[id='add-to-cart-test.allthethings()-t-shirt-(red)']")

    @property
    def cart(self):
        return self.page.locator(".shopping_cart_container")

    @property
    def burger_menu(self):
        return self.page.locator("#react-burger-menu-btn")

    @property
    def sort_menu(self):
        return self.page.locator("[data-test='product_sort_container']")

    @property
    def item_titles(self):
        return self.page.locator(".inventory_item_name")

    def navigate(self):
        self.page.goto("https://www.saucedemo.com/inventory.html")

    def select_sort_option(self, *args, **kwargs):
        sort_list = self.page.query_selector("[data-test='product_sort_container']")
        sort_list.select_option(*args, **kwargs)
